document.getElementById("login-form").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent form submission
  
    // Get username and password values
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
  
    // Perform login logic here (e.g., send login request to server)
  
    // For demonstration purposes, display the username and password in the console
    console.log("Username: " + username);
    console.log("Password: " + password);
  
    // You can redirect the user to another page or perform other actions after successful login
  });
  